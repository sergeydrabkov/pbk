﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using pbk.Data;
using pbk.Models;

namespace pbk.Controllers
{
    public class SubmissionAnswersController : Controller
    {
        private readonly pbkContext _context;

        public SubmissionAnswersController(pbkContext context)
        {
            _context = context;
        }

        // GET: SubmissionAnswers
        public async Task<IActionResult> Index()
        {
            var pbkContext = _context.SubmissionAnswer.Include(s => s.Answer);
            return View(await pbkContext.ToListAsync());
        }

        // GET: SubmissionAnswers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var submissionAnswer = await _context.SubmissionAnswer
                .Include(s => s.Answer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (submissionAnswer == null)
            {
                return NotFound();
            }

            return View(submissionAnswer);
        }

        // GET: SubmissionAnswers/Create
        public IActionResult Create()
        {
            ViewData["AnswerId"] = new SelectList(_context.Answer, "Id", "Content");
            return View();
        }

        // POST: SubmissionAnswers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AnswerId,Id,Content")] SubmissionAnswer submissionAnswer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(submissionAnswer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AnswerId"] = new SelectList(_context.Answer, "Id", "Content", submissionAnswer.AnswerId);
            return View(submissionAnswer);
        }

        // GET: SubmissionAnswers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var submissionAnswer = await _context.SubmissionAnswer.FindAsync(id);
            if (submissionAnswer == null)
            {
                return NotFound();
            }
            ViewData["AnswerId"] = new SelectList(_context.Answer, "Id", "Content", submissionAnswer.AnswerId);
            return View(submissionAnswer);
        }

        // POST: SubmissionAnswers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AnswerId,Id,Content")] SubmissionAnswer submissionAnswer)
        {
            if (id != submissionAnswer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(submissionAnswer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubmissionAnswerExists(submissionAnswer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AnswerId"] = new SelectList(_context.Answer, "Id", "Content", submissionAnswer.AnswerId);
            return View(submissionAnswer);
        }

        // GET: SubmissionAnswers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var submissionAnswer = await _context.SubmissionAnswer
                .Include(s => s.Answer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (submissionAnswer == null)
            {
                return NotFound();
            }

            return View(submissionAnswer);
        }

        // POST: SubmissionAnswers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var submissionAnswer = await _context.SubmissionAnswer.FindAsync(id);
            _context.SubmissionAnswer.Remove(submissionAnswer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubmissionAnswerExists(int id)
        {
            return _context.SubmissionAnswer.Any(e => e.Id == id);
        }
    }
}
