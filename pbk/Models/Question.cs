﻿using pbk.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models
{
    public class Question
    {
        private ICollection<Answer> answers;

        public Question()
        {
            //this.TestId = testId;
            this.answers = new HashSet<Answer>();
        }

        //public Question(int testId, string condition) : this(testId)
        public Question(string condition) : this()
        {
            this.Condition = condition;
        }

        public int Id { get; set; }

        [Required]
        public string Condition { get; set; }

        [Display(Name = "Test")]
        public int TestId { get; set; }

        public Test Test { get; set; }

        [Required]
        public QuestionType Type { get; set; }

        public virtual ICollection<Answer> Answers
        {
            get { return this.answers; }
            set { this.answers = value; }
        }
    }
}
