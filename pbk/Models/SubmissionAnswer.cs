﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models
{
    public class SubmissionAnswer
    {
        public SubmissionAnswer()
        {

        }

        public SubmissionAnswer(int answerId, string content)
        {
            this.AnswerId = answerId;            
            this.Content = content;
        }

        public int? AnswerId { get; set; }

        [ForeignKey("AnswerId")]
        public Answer Answer { get; set; }

        public int Id { get; set; }

        public string Content { get; set; }
    }
}
