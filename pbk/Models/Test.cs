﻿using pbk.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models
{
    public class Test
    {
        private ICollection<Question> questions;
        private ICollection<Test> children;
        
        public Test()
        {
            
            this.questions = new HashSet<Question>();
            this.children = new HashSet<Test>();
        }

        public Test(ICollection<Question> questions, TestType type, uint givenTime) : this()
        {
            this.questions = questions;
            this.Type = type;
            this.GivenTime = givenTime;
        }

        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public TestType Type { get; set; }

        [Display(Name = "Given time in seconds")]
        public uint? GivenTime { get; set; }

        [Display(Name = "Parent test")]
        public int? ParentId { get; set; }

        public Test Parent { get; set; }

        public virtual ICollection<Question> Questions
        {
            get { return this.questions; }
            set { this.questions = value; }
        }

        public virtual ICollection<Test> Children
        {
            get { return this.children; }
            set { this.children = value; }
        }      
    }
}
