﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models
{
    public class Answer
    {
        public Answer()
        {
        }

        public Answer(string content, bool isCorrect) : this()
        {
            this.Content = content;
            this.IsCorrect = isCorrect;
        }

        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Display(Name = "Correct answer")]
        public bool IsCorrect { get; set; }

        public int QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }
    }
}
