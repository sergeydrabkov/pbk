﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models.Enums
{
    public enum TestType
    {
        [Display(Name = "Show right answer after question")]
        ShowRightAnswerAfterQuestion = 0,
        [Display(Name = "Show right answers after test")]
        ShowRightAnswersAfterTest = 1
    }
}
