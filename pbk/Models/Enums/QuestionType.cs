﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models.Enums
{
    public enum QuestionType
    {
        [Display(Name = "Many right answers")]
        ManyRightAnswers = 0,
        [Display(Name = "One right answer")]
        OneRightAnswer = 1,
        [Display(Name = "Need enter right answer")]
        NeedEnterRightAnswer = 2
    }
}
