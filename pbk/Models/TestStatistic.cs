﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pbk.Models
{
    public class TestStatistic
    {
        public TestStatistic()
        {

        }

        public TestStatistic(int testId)
        {
            this.TestId = testId;
        }

        public int Id { get; set; }

        public int Total { get; set; }

        public int Correct { get; set; }

        public int TestId { get; set; }

        [ForeignKey("TestId")]
        public virtual Test Test { get; set; }
    }
}

