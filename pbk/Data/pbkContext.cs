﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pbk.Models;

namespace pbk.Data
{
    public class pbkContext : DbContext
    {
        public pbkContext (DbContextOptions<pbkContext> options)
            : base(options)
        {
        }

        public DbSet<pbk.Models.Test> Test { get; set; }

        public DbSet<pbk.Models.Question> Question { get; set; }

        public DbSet<pbk.Models.Answer> Answer { get; set; }      

        public DbSet<pbk.Models.SubmissionAnswer> SubmissionAnswer { get; set; }
    }
}
